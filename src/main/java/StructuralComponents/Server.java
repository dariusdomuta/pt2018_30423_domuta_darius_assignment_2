package StructuralComponents;

import Constants.Constants;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server implements Runnable {
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private int serverId;
    private Task currentTask;

    public Task getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public Server(){
        this.tasks = new ArrayBlockingQueue<Task>(Constants.MAX_TASKS_PER_SERVER);
        this.waitingPeriod = new AtomicInteger();
        this.serverId = (int) (Math.random()*10000+1);
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public int getServerId() {
        return this.serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public void addTask(Task newTask) {

        this.tasks.add(newTask);
        waitingPeriod.addAndGet(newTask.getProcessingTime());
        System.out.println("Server:" + this.serverId +": New TASK was added, processing time: " + newTask.getProcessingTime() + "total processing time " + waitingPeriod);
    }

    public void run() {
        while(true){
            try {
                if (!this.tasks.isEmpty()){
                    currentTask = tasks.take();
                    int waitTime = currentTask.getProcessingTime();

                    System.out.println("Server " + this.getServerId() + " is processing the task AT: " + currentTask.getArrivalTime() + " PT " + waitTime );
                    Thread.sleep(waitTime*1000);
                    this.waitingPeriod.addAndGet(waitTime*(-1));
                    System.out.println("Task successfully processed, with PT: " + waitTime + " and AT: " + currentTask.getArrivalTime());
                    currentTask=null;
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("Intrerrupted exception");
            }
        }
    }

    public BlockingQueue<Task> getTasks() {
        return this.tasks;
    }
}
