package StructuralComponents;

import Strategies.ConcreteStrategyQueue;
import Strategies.ConcreteStrategyTime;
import Strategies.Strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy = new ConcreteStrategyQueue();

    private void checkServerId(Server server){
        for(Server existentServer:servers){
            if (existentServer.getServerId() == server.getServerId()){
                server.setServerId((int)(Math.random()*10000+1));
                checkServerId(server);
                break;
            }
        }
    }

    public enum SelectionPolicy {
        SHORTEST_QUEUE, SHORTEST_TIME
    }

    public Scheduler(int maxNoServers, int maxTasksPerServer) {
        this.servers = new ArrayList<Server>();
        for(int i=0; i<maxNoServers; i++) {
            try{
                Server server = new Server();
                System.out.println("New server was added, server id: "+ server.getServerId());
                this.servers.add(server);
                Thread thread = new Thread(server);
            } catch(NullPointerException e){
                System.out.println("schedule error");
            }


        }
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
    }

    public void placeTask(Task task) {
        strategy.addTask(servers,task);
    }

    public List<Server> getServers() {
        return this.servers;
    }
}
