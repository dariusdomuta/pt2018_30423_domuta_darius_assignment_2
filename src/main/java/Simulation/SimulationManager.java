package Simulation;

import Comparators.TaskComparatorByArrival;
import Constants.Constants;
import GUI.SimulatorFrame;
import StructuralComponents.Scheduler;
import StructuralComponents.Task;
import com.sun.javaws.exceptions.ErrorCodeResponseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class SimulationManager implements Runnable {

    public int timeLimit = 100;
    public int maxProcessingTime = 6;
    public int minProcessingTime = 2;
    public static int numberOfServers = 3;
    public int numberOfClients = 100;
    public Scheduler.SelectionPolicy selectionPolicy = Scheduler.SelectionPolicy.SHORTEST_TIME;

    private Scheduler scheduler;
    private SimulatorFrame frame = new SimulatorFrame();
    private List<Task> generatedTasks = new ArrayList<Task>();

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public SimulatorFrame getFrame() {
        return frame;
    }

    public void setFrame(SimulatorFrame frame) {
        this.frame = frame;
    }

    public List<Task> getGeneratedTasks() {
        return generatedTasks;
    }

    public void setGeneratedTasks(List<Task> generatedTasks) {
        this.generatedTasks = generatedTasks;
    }

    public SimulationManager(){

            if (numberOfServers <= Constants.MAX_NO_SERVERS) {
                scheduler = new Scheduler(numberOfServers,Constants.MAX_TASKS_PER_SERVER);
            } else {
                System.out.println("Maximum number of servers allowes is " + Constants.MAX_NO_SERVERS + "!!!");
            }

            for (int i=0; i<scheduler.getServers().size();i++) {
                Thread thread = new Thread(scheduler.getServers().get(i));
                thread.start();
            }
            generateNRandomTasks();
            scheduler.changeStrategy(selectionPolicy);
    }

    private void generateNRandomTasks() {
        for (int i = 0; i<numberOfClients;i++) {
            int arrivalTime = (int) (Math.random() * timeLimit) +1;
            int processingTime = (int) (Math.random() * (maxProcessingTime-1)) +minProcessingTime;
            Task task = new Task(arrivalTime,processingTime);

            this.generatedTasks.add(task);
        }
        Collections.sort(this.generatedTasks, new TaskComparatorByArrival());
        for (Task task: generatedTasks) {
            System.out.println("arrival time " + task.getArrivalTime() + "processing time " + task.getProcessingTime());
        }

    }

    public void run() {
        int currentTime = 0;

        System.out.println("Thread started to run");
        while (currentTime < timeLimit) {
            currentTime++;
            System.out.println("current time: " + currentTime);
            try{
                for (Task task: generatedTasks){
                    if (task.getArrivalTime() == currentTime) {
                        if (task.getArrivalTime() > currentTime) break;
                        scheduler.placeTask(task);
                    }

                }
            } catch (NullPointerException e){
                System.out.println("null pointer at sim run");
            }
            frame.displayData(scheduler.getServers());
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("sleep intrerrupt");
            }

        }
    }
    public static void main(String[] args){

        SimulationManager gen = new SimulationManager();
        Thread thread = new Thread(gen);
        thread.start();

    }
}
