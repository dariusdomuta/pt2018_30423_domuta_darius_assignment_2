package Constants;

public class Constants {
    public static final int MAX_NO_SERVERS = 20;
    public static final int MAX_WAIT_TIME_PER_SERVER = 100;
    public static final int MAX_TASKS_PER_SERVER = 20;
}
