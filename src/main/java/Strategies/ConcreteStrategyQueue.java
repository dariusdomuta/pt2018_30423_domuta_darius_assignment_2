package Strategies;

import Constants.Constants;
import StructuralComponents.Server;
import StructuralComponents.Task;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy {
    public void addTask(List<Server> servers, Task task) {
        int minQueueSize = Constants.MAX_TASKS_PER_SERVER;
        for (Server server: servers){
            if(server.getTasks().size() < minQueueSize){
                minQueueSize = server.getTasks().size();
            }
        }

        for (Server server: servers){
            if(server.getTasks().size() == minQueueSize){
                System.out.println("Server " + server.getServerId() + "task arrival time " + task.getArrivalTime() + "processing time:" + task.getProcessingTime());
                server.addTask(task);
                break;
            }
        }
    }
}
