package Strategies;

import StructuralComponents.Server;
import StructuralComponents.Task;

import java.util.List;

public interface Strategy {
    public void addTask(List<Server> servers, Task task);

}
