package Strategies;

import Constants.Constants;
import StructuralComponents.Server;
import StructuralComponents.Task;

import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    public void addTask(List<Server> servers, Task task) {
        int minWaitTime = Constants.MAX_WAIT_TIME_PER_SERVER;
        for (Server server: servers){
            if(server.getWaitingPeriod().get() < minWaitTime){
                minWaitTime = server.getWaitingPeriod().get();
            }
        }

        for (Server server: servers){
            if(server.getWaitingPeriod().get() == minWaitTime){
                server.addTask(task);
                break;
            }
        }
    }
}
