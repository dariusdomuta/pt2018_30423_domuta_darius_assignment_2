package Comparators;

import StructuralComponents.Task;

import java.util.Comparator;

public class TaskComparatorByArrival implements Comparator<Task> {

    public int compare(Task o1, Task o2) {
        return o1.getArrivalTime()-o2.getArrivalTime();
    }
}
