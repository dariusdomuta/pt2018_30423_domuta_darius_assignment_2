package GUI;

import Simulation.SimulationManager;
import StructuralComponents.Server;
import StructuralComponents.Task;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class SimulatorFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel panel;
    private int WIDTH = 600, HEIGHT = 600;

    public SimulatorFrame() {
        panel = new JPanel();
        this.add(panel);
        panel.setLayout(null);
        this.setSize(WIDTH, HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }

    public void displayData(List<Server> servers) {
        int nrOfServers = servers.size();
        panel.removeAll();
        panel.revalidate();
        panel.setLayout(null);
        int i=0;
        for (Server server: servers) {
            i++;
            JLabel serverLabel = new JLabel("Server"+i);
            serverLabel.setBounds(10, 10*i*10, 100, 40);
            serverLabel.setSize(100,40);
            panel.add(serverLabel);

            JLabel currentServerTask = new JLabel("Current client:");
            currentServerTask.setBounds(10, 10*i*10+20, 100, 40);
            currentServerTask.setSize(100, 40);
            panel.add(currentServerTask);

            if (server.getCurrentTask() != null) {
                JLabel currentServerTaskData = new JLabel("AT:" + server.getCurrentTask().getArrivalTime() + " WT:" + server.getCurrentTask().getProcessingTime());
                currentServerTaskData.setBounds(10, 10*i*10+40, 100, 40);
                currentServerTaskData.setSize(100, 40);
                panel.add(currentServerTaskData);
            }



            int j = 0;
            for (Task task: server.getTasks()){
                j++;
                JLabel taskLabel = new JLabel("Client AT:" + task.getArrivalTime() + " WT:" + task.getProcessingTime());
                taskLabel.setBounds(j*120, 10*i*10, 100, 40);
                taskLabel.setSize(100,40);
                panel.add(taskLabel);
            }
        }





        panel.repaint();
        panel.revalidate();
    }

}
